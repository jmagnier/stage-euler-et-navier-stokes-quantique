import numpy as np
from scipy.sparse import diags, bmat, eye
from scipy.sparse.linalg import spsolve
from matplotlib import pyplot as plt

"""
Simulation de Navier Stokes quantique 1D
"""

# Constantes du système
gamma = 1.4
epsilon = 3
nu = 1

# Paramètre de la densite initiale minimale
inf_rho = 1

# Condition limite
periodique = True # si False la condition est de Dirichlet sur rho, et v avec les valeurs fixes aux bords stockés dans W

# Domaine de calcul
x_debut = 0; x_fin = 30
T = 1

# Paramètres du schéma
N = 1000
CFL = 0.1

# Paramètres d'affichage
anim = True # True / False
enregistrer_anim = True # True / False
anim_nb_img = 100 # Nombre d'images à afficher durant toute la simulation
tps_pause = 0.01 # Temps d'attente après l'affichage de la solution en seconde


def flux_rusanov(W):
    """
    Calcule le flux de Rusanov associé à W
    """
    u = W[1,:] / W[0,:]
    FW = np.zeros(np.shape(W)) # FW = F(W)
    FW[0,:] = W[1,:]
    FW[1,:] = u * W[1,:] + np.power(W[0,:],gamma)
    FW[2,:] = u * W[2,:]
    
    max_abs_lambda = np.abs(u) + np.sqrt(gamma) * np.power(W[0,:], 0.5*(gamma-1))
    lambda_rusanov = np.amax([max_abs_lambda[:-1], max_abs_lambda[1:]], axis=0)

    F = 0.5*(FW[:,1:] + FW[:,:-1]) - lambda_rusanov * (0.5 * (W[:,1:] - W[:,:-1]))
    return F


if (__name__ == "__main__"):
    # Initialisation
    dx = (x_fin-x_debut)/N
    X = np.linspace(x_debut+dx/2,x_fin-dx/2,N)
    x_milieu = 0.5*(x_debut+x_fin)

    rho = inf_rho+np.exp(-(X-x_milieu)**2)
    dx_rho = -2*(X-x_milieu)*np.exp(-(X-x_milieu)**2)
    u = np.zeros(N)
    W = np.zeros([3,N])
    W[0,:] = rho
    W[1,:] = rho * u
    W[2,:] = epsilon*dx_rho
    
    # Energie initiale
    v = W[2,:] / W[0,:]
    energie = [dx * np.sum( 0.5*W[0,:]*(u*u+v*v) + np.power(W[0,:], gamma)/(gamma-1))]
    
    if anim :
        # Affichage initiale
        plt.ion()
        fig, [ax1,ax2,ax3,ax4] = plt.subplots(1,4)
        ax1.set_title("Densite : rho")
        ax2.set_title("Vitesse : u")
        ax3.set_title("v = epsilon*dx(rho)/rho")
        ax4.set_title("Energie")
        plot1, = ax1.plot(X,W[0,:],"-b")
        plot2, = ax2.plot(X,u,"-b")
        plot3, = ax3.plot(X,v,"-b")
        plt.pause(tps_pause)
    if enregistrer_anim :
        data_anim = [X,W[0]]
    nb_img = 1
    
    t = 0
    temps = [t]
    min_rho0 = min(W[0])
    min_rho = min_rho0
    while(t<T) :
        dt=CFL*dx/(2*np.max(np.abs(W[1,:]/W[0,:])+np.sqrt(gamma)*np.power(W[0,:], 0.5*(gamma-1))))
        t += dt
        temps.append(t)
        
        
        # Phase hyperbolique
        if periodique :
            F = flux_rusanov(np.column_stack((W[:,-1],W,W[:,0])))
            W = W - (dt/dx)*(F[:, 1:] - F[:, :-1])
        else :
            F = flux_rusanov(W)
            W[:, 1:N-1] = W[:, 1:N-1] - (dt/dx)*(F[:, 1:] - F[:, :-1])
        

        # Phase de diffusion
        cste_dxdteps = epsilon*dt/(2*dx*dx)
        cste_dxdtnu = nu*dt/(dx*dx)
        if periodique :
            rho = np.hstack((W[0,-1],W[0,:],W[0,0]))
            diag_inf = (rho[1:-2] + rho[2:-1]) / rho[1:-2]
            diag_mid = -(rho[:-2] + 2*rho[1:-1] + rho[2:]) / rho[1:-1]
            diag_sup = (rho[1:-2] + rho[2:-1]) / rho[2:-1]
            A1 = diags([diag_inf, diag_mid, diag_sup], [-1,0,1], format='lil')
            A1[0,-1] = (rho[0] + rho[1]) / rho[0]
            A1[-1,0] = (rho[-2] + rho[-1]) / rho[-1]
            M = bmat([[eye(N) - cste_dxdtnu * A1, -cste_dxdteps * A1],
                      [cste_dxdteps * A1, eye(N)]],
                     format="csr")
            Y = np.concatenate((W[1,:], W[2,:]))
            tmp = spsolve(M,Y)
            W[1,:] = tmp[:N]
            W[2,:] = tmp[N:]
        else :
            rho = W[0,:]
            diag_inf = (rho[1:-2] + rho[2:-1]) / rho[1:-2]
            diag_mid = -(rho[:-2] + 2*rho[1:-1] + rho[2:]) / rho[1:-1]
            diag_sup = (rho[1:-2] + rho[2:-1])/ rho[2:-1]
            A1 = diags([diag_inf, diag_mid, diag_sup], [-1,0,1], format='lil')
            M = bmat([[eye(N-2) - cste_dxdtnu * A1, -cste_dxdteps * A1],
                      [cste_dxdteps * A1, eye(N-2)]],
                     format="csr")
            Y = np.concatenate((W[1,1:-1], W[2,1:-1]))
            tmp = spsolve(M,Y)
            W[1,1:-1] = tmp[:N-2]
            W[2,1:-1] = tmp[N-2:]


        # Calcul d'energie
        u = W[1,:] / W[0,:]
        v = W[2,:] / W[0,:]
        energie.append( dx * np.sum(0.5*W[0,:]*(u*u+v*v) + np.power(W[0,:], gamma)/(gamma-1))) 
        
        # Calcul du minimum de la densité durant toute la simulation
        min_rho = min(min(W[0]), min_rho)

        # Affichage du resultat
        if t >= (nb_img*T/anim_nb_img):
            if anim :
                u = W[1,:] / W[0,:]
                v = W[2,:] / W[0,:]
                plot1.set_ydata(W[0,:])
                plot2.set_ydata(u)
                plot3.set_ydata(v)
                min_rho = min(W[0,:]); max_rho = max(W[0,:])
                ax1.set_ylim([min_rho-0.05*(max_rho-min_rho), max_rho+0.05*(max_rho-min_rho)])
                min_u = min(u); max_u = max(u)
                ax2.set_ylim([min_u-0.05*(max_u-min_u), max_u+0.05*(max_u-min_u)])
                min_v = min(v); max_v = max(v)
                ax3.set_ylim([min_v-0.05*(max_v-min_v), max_v+0.05*(max_v-min_v)])
                nb_img += 1
                plt.pause(tps_pause)
            if enregistrer_anim :
                data_anim.append(W[0])
            nb_img += 1

    # Affichage du resultat final
    u = W[1,:] / W[0,:]
    v = W[2,:] / W[0,:]
    if anim : 
        plot1.set_ydata(W[0,:])
        plot2.set_ydata(u)
        plot3.set_ydata(v)
        ax4.plot(temps,energie,"-b")
        min_rho = min(W[0,:]); max_rho = max(W[0,:])
        ax1.set_ylim([min_rho-0.05*(max_rho-min_rho), max_rho+0.05*(max_rho-min_rho)])
        min_u = min(u); max_u = max(u)
        ax2.set_ylim([min_u-0.05*(max_u-min_u), max_u+0.05*(max_u-min_u)])
        min_v = min(v); max_v = max(v)
        ax3.set_ylim([min_v-0.05*(max_v-min_v), max_v+0.05*(max_v-min_v)])
        ax4.set_ylim([min(energie), max(energie)])
        plt.show(block=True)
    else : 
        plt.ioff()
        plt.figure()
        plt.subplot(1,4,1)
        plt.plot(X, W[0,:], "b")
        plt.title("Densite : rho")
        plt.subplot(1,4,2)
        plt.plot(X, u, "b")
        plt.title("Vitesse : u")
        plt.subplot(1,4,3)
        plt.plot(X, v, "b")
        plt.title("v = epsilon*dx(rho)/rho")
        plt.subplot(1,4,4)
        plt.plot(temps, energie, "b" )
        plt.title("Energie")
        plt.show()

    if enregistrer_anim :
        data_anim.append(W[0])
        np.save("data/NSQ1D_N{:d}_eps{:.0f}_nu{:.0f}".format(N,epsilon,nu), data_anim)  

    print("Minimum de la densité à l'instant initial : {:e}".format(min_rho0))
    print("Minimum de la densité atteinte durant la simulation : {:e}".format(min_rho))