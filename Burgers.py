import numpy as np
from matplotlib import pyplot as plt

"""
Simulation du problème de Riemann pour les équations de Burgers
"""

# Etats de la solution initiale
ug = float(-1); ud = float(1)
x_discontinu = 0
# Domaine de calcul
x_debut = -2; x_fin = 2
T = 1
# Paramètres du schéma
N = 100
dx = (x_fin-x_debut)/N
CFL = 1


def flux_godunov(ug,ud):
    flux = np.nan
    if(ug <= ud):
        if(ud <= 0):
            flux = 0.5*ud*ud
        elif(ug <= 0):
            flux = 0
        else:
            flux = 0.5*ug*ug
    else:
        if(ug <= 0):
            flux = 0.5*ud*ud
        elif(ud <= 0):
            flux = 0.5*max(ug*ug,ud*ud)
        else:
            flux = 0.5*ug*ug
    return flux


def flux_roe(ug,ud):
    return 0.5*(0.5*ug*ug + 0.5*ud*ud) - 0.5*abs(0.5*(ug+ud))*(ud-ug)


def flux_roe_correction(ug,ud):
    epsilon = 1e-10
    lambda_mod = abs(0.5*(ug+ud))
    if (lambda_mod < epsilon):
        lambda_mod = max(0, lambda_mod - ug, ud - lambda_mod)
    return 0.5*(0.5*ug*ug + 0.5*ud*ud) - 0.5*lambda_mod*(ud-ug)


def flux_rusanov(ug,ud):
    return 0.5*(0.5*ug*ug + 0.5*ud*ud) - 0.5*max(abs(ug),abs(ud))*(ud-ug)


def flux(ug,ud,choix_flux) :
    if (choix_flux == 1):
        return flux_roe(ug,ud)
    elif (choix_flux == 2):
        return flux_roe_correction(ug,ud)
    elif (choix_flux == 3):
        return flux_rusanov(ug,ud)
    else:
        return flux_godunov(ug,ud)

if (__name__ == "__main__"):
    # Initialisation
    X = np.linspace(x_debut+dx/2, x_fin-dx/2, N)
    U0 = np.where(X<x_discontinu, ug, ud)
    U = U0.copy()
    N = len(X)
    F = np.zeros(N-1)
    
    # Choix du flux
    choix_flux = int(input("Choix du flux :\n0 : Godunov\n1 : Roe sans correction entropique\n2 : Roe avec correction entropique\n3 : Rusanov\nchoix : "))
    if (choix_flux == 1):
        titre = "Flux de Roe sans correction entropique"
    elif (choix_flux == 2):
        titre = "Flux de Roe avec correction entropique"
    elif (choix_flux == 3):
        titre = "Flux de Rusanov"
    else : 
        titre = "Flux de Godunov"
    
    # Initialisation de l'affichage
    plt.close()
    plt.ion()
    fig, ax=plt.subplots(1,1)
    plt.plot(X,U0,"b-")
    plt.annotate("t = 0.00",[x_debut,ud])
    plt.title(titre)
    plt.pause(0.5)
    
    t = 0
    while t < T :
        dt = dx*CFL / (2*np.max(U))
        t = t+dt
        # Calcul des nouveaux flux
        for i in range(0,N-1):
            F[i] = flux(U[i], U[i+1], choix_flux)
        
        # Calcul de la solution numerique
        U[1:N-1] = U[1:N-1] - (dt/dx)*(F[1:] - F[:-1])
        
        plt.cla()
        # Affichage de la solution exacte
        if (ug<ud) : #onde de detente
            plt.plot([x_debut, x_discontinu+ug*t, x_discontinu+ud*t, x_fin],[ug,ug,ud,ud],"-r")
        if (ug>=ud) : #onde de choc
            v_hugoniot = 0.5*(ug+ud)
            plt.plot([x_debut,x_discontinu + v_hugoniot*t, x_discontinu + v_hugoniot*t, x_fin],[ug,ug,ud,ud],"-r")
        
        plt.plot(X,U,"b*")
        plt.annotate("t = {0:0.2f}".format(t), [x_debut,ud])
        plt.legend(["solution exacte", "solution numerique"], loc = 1+3*(ug<=ud))
        plt.title(titre)
        plt.pause(0.001)
    
    plt.show(block=True)
    