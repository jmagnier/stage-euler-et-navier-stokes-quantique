import numpy as np
from scipy.sparse import diags, lil_matrix
from scipy.sparse.linalg import spsolve
from matplotlib import pyplot as plt

"""
Simulation de Shallow Water sans capillarité, avec capillarité linearisée et avec capillarité non linearisée
"""


#Constantes du système
sigma = 0.0728 # N/m
rho = 1000 # kg/m^3
nu = 1e-6 # m^2/s
gr = 9.81 # m/s^2
h1 = 2.725e-3 # m
h0 = h1
b0 = 4.29193
b = 1.5 * h1

#Domaine de calcul
x_debut = -50e-3; x_fin = 50e-3 # m
T = 1.5e-2 # s

#Parametres du schéma
N = 1000
CFL = 0.01

def flux_rusanov(W):
    """
    Calcule le flux de Rusanov associe a W
    """
    W_etendu = np.column_stack((W[:,-1],W,W[:,0]))
    u = W_etendu[1,:] / W_etendu[0,:]
    FW = np.zeros(np.shape(W_etendu)) # FW = F(W)
    FW[0,:] = W_etendu[1,:]
    FW[1,:] = u * W_etendu[1,:] + 0.5*gr*(W_etendu[0,:] * W_etendu[0,:])
    max_abs_lambda = np.abs(u) + np.sqrt( gr * W_etendu[0,:])
    lambda_rusanov = np.amax([max_abs_lambda[:-1], max_abs_lambda[1:]], axis=0)
    F = 0.5*(FW[:,1:] + FW[:,:-1]) - lambda_rusanov * (0.5 * (W_etendu[:,1:] - W_etendu[:,:-1]))
    return F


if (__name__ == "__main__"):
    # Initialisation
    dx = (x_fin-x_debut)/N
    X = np.linspace(x_debut+dx/2,x_fin-dx/2,N)
    h = h0 + h1 * np.exp(- (X*X)/(2*((b/b0)**2)))
    dx_h = h1 * (-X/((b/b0)**2)) * np.exp(- (X*X)/(2*((b/b0)**2)))
    u = np.zeros(N)
    plt.ioff()
    fig, [ax1,ax2,ax3,ax4] = plt.subplots(1,4)
    
    # Affichage de la densité initiale
    ax1.plot(1000*X,1000*h)
    ax1.fill_between(1000*X, 1000*h, color='#539ecd')
    ax1.set_xlim([1000*x_debut, 1000*x_fin])
    ax1.set_ylim([0, 5.5])
    ax1.set_xlabel("x (mm)")
    ax1.set_ylabel("h (mm)")

    # Shallow Water sans capillarité
    W = np.zeros([2,N])
    W[0,:] = h
    W[1,:] = h * u
    t = 0
    temps = [t]
    while(t<T) :
        dt=CFL*dx/(2* np.max(np.abs(W[1,:]/W[0,:]) + np.sqrt( gr * W[0,:])) )
        t += dt
        temps.append(t)
        F = flux_rusanov(W)
        W = W - (dt/dx)*(F[:, 1:] - F[:, :-1])
    
    ax2.plot(1000*X,1000*W[0,:])
    ax2.fill_between(1000*X, 1000*W[0,:], color='#539ecd')
    ax2.set_xlim([1000*x_debut, 1000*x_fin])
    ax2.set_ylim([0, 5.5])
    ax2.set_xlabel("x (mm)")
    ax2.set_ylabel("h (mm)")
    
    
    # Shallow avec capillarité linearisée
    h = h0 + h1 * np.exp(- (X*X)/(2*((b/b0)**2)))
    dx_h = h1 * (-X/((b/b0)**2)) * np.exp(- (X*X)/(2*((b/b0)**2)))
    u = np.zeros(N)
    W = np.zeros([3,N])
    W[0,:] = h
    W[1,:] = h * u
    W[2,:] = np.sqrt(sigma/rho) * np.sqrt(h) * dx_h
    t = 0
    temps = [t]
    while(t<T) :
        dt=CFL*dx/(2* np.max(np.abs(W[1,:]/W[0,:]) + np.sqrt( gr * W[0,:])) )
        t += dt
        temps.append(t)
        #Phase hyperbolique
        F = flux_rusanov(W)
        W = W - (dt/dx)*(F[:, 1:] - F[:, :-1])
        
        #Phase de diffusion
        W_etendu = np.column_stack((W[:,-1],W,W[:,0]))
        h = W_etendu[0,:]
        u = W_etendu[1,:] / W_etendu[0,:]
        v = W_etendu[2,:] / W_etendu[0,:]
        f = np.sqrt(sigma/rho) * np.sqrt(h)
        g = 0.5 * h * v
        
        diag1 = -dt*( -(h[1:-2]+h[2:-1])*f[2:-1]/(2*dx*dx) + g[2:-1]/(2*dx) ) / h[1:-2]
        diag2 = -dt*( (h[:-2] + 2*h[1:-1] + h[2:])*f[1:-1]/(2*dx*dx) ) / h[1:-1] 
        diag3 = np.concatenate(([0], -dt*( -(h[1:-2]+h[2:-1])*f[1:-2]/(2*dx*dx) - g[1:-2]/(2*dx) ) / h[2:-1]  ,  [0]))
        diag4 = np.ones(2*(N))
        diag5 = np.concatenate(([0],  -dt*( (h[1:-2]+h[2:-1])*f[1:-2]/(2*dx*dx) + g[1:-2]/(2*dx) ) / h[1:-2],  [0]))
        diag6 = -dt*( -(h[:-2] + 2*h[1:-1] + h[2:])*f[1:-1]/(2*dx*dx) ) / h[1:-1]           
        diag7 = -dt*( (h[1:-2]+h[2:-1])*f[2:-1]/(2*dx*dx) - g[2:-1]/(2*dx) ) / h[2:-1]
        A = diags([diag1, diag2, diag3, diag4, diag5, diag6, diag7],
                  [-(N+1),-N,-(N-1),0,N-1,N,N+1])
        A = lil_matrix(A)
        A[-1,0] =  -dt*(-(h[-2]+h[1])*f[-2]/(2*dx*dx) - g[-2]/(2*dx) ) / h[1]
        A[N,N-1] = -dt*(-(h[-2]+h[1])*f[1]/(2*dx*dx) + g[1]/(2*dx) ) / h[-2]
        A[N-1,N] = -dt*( (h[-2]+h[1])*f[1]/(2*dx*dx) - g[1]/(2*dx) ) / h[1] 
        A[0,-1] = -dt*( (h[-2]+h[1])*f[-2]/(2*dx*dx) + g[-2]/(2*dx) ) / h[-2]
        A = A.tocsr()
        Y = np.concatenate((W[1,:], W[2,:]))
        tmp = spsolve(A,Y)
        W[1,:] = tmp[:N]
        W[2,:] = tmp[N:]

    ax3.plot(1000*X,1000*W[0,:])
    ax3.fill_between(1000*X, 1000*W[0,:], color='#539ecd')
    ax3.set_xlim([1000*x_debut, 1000*x_fin])
    ax3.set_ylim([0, 5.5])
    ax3.set_xlabel("x (mm)")
    ax3.set_ylabel("h (mm)")
    
    
    # Shallow avec capillarité non linearisée
    h = h0 + h1 * np.exp(- (X*X)/(2*((b/b0)**2)))
    dx_h = h1 * (-X/((b/b0)**2)) * np.exp(- (X*X)/(2*((b/b0)**2)))
    u = np.zeros(N)
    W = np.zeros([3,N])
    W[0,:] = h
    W[1,:] = h * u
    W[2,:] = (np.sqrt(2) / np.sqrt(1+np.sqrt(1+dx_h*dx_h))) * np.sqrt(sigma/rho) * np.sqrt(h) * dx_h
    t = 0
    temps = [t]
    while(t<T) :
        dt=CFL*dx/(2* np.max(np.abs(W[1,:]/W[0,:]) + np.sqrt( gr * W[0,:])) )
        t += dt
        temps.append(t)
        #Phase hyperbolique
        F = flux_rusanov(W)
        W = W - (dt/dx)*(F[:, 1:] - F[:, :-1])
        
        #Phase de diffusion
        W_etendu = np.column_stack((W[:,-1],W,W[:,0]))
        h = W_etendu[0,:]
        u = W_etendu[1,:] / W_etendu[0,:]
        v = W_etendu[2,:] / W_etendu[0,:]
        f = np.sqrt(sigma/rho) * np.sqrt(h) / np.sqrt(1+(rho*h)/(4*sigma)*(v*v)) * (1 - (rho*h) / (4*sigma) * (v*v) / (1 + (rho*h)/(2*sigma)*(v*v)))
        g = 0.5 * h * v / (1+(rho*h)/(2*sigma)*(v*v))
        
        diag1 = -dt*( -(h[1:-2]+h[2:-1])*f[2:-1]/(2*dx*dx) + g[2:-1]/(2*dx) ) / h[1:-2]
        diag2 = -dt*( (h[:-2] + 2*h[1:-1] + h[2:])*f[1:-1]/(2*dx*dx) ) / h[1:-1] 
        diag3 = np.concatenate(([0], -dt*( -(h[1:-2]+h[2:-1])*f[1:-2]/(2*dx*dx) - g[1:-2]/(2*dx) ) / h[2:-1]  ,  [0]))
        diag4 = np.ones(2*(N))
        diag5 = np.concatenate(([0],  -dt*( (h[1:-2]+h[2:-1])*f[1:-2]/(2*dx*dx) + g[1:-2]/(2*dx) ) / h[1:-2],  [0]))
        diag6 = -dt*( -(h[:-2] + 2*h[1:-1] + h[2:])*f[1:-1]/(2*dx*dx) ) / h[1:-1]           
        diag7 = -dt*( (h[1:-2]+h[2:-1])*f[2:-1]/(2*dx*dx) - g[2:-1]/(2*dx) ) / h[2:-1]
        A = diags([diag1, diag2, diag3, diag4, diag5, diag6, diag7],
                  [-(N+1),-N,-(N-1),0,N-1,N,N+1])
        A = lil_matrix(A)
        A[-1,0] =  -dt*(-(h[-2]+h[1])*f[-2]/(2*dx*dx) - g[-2]/(2*dx) ) / h[1]
        A[N,N-1] = -dt*(-(h[-2]+h[1])*f[1]/(2*dx*dx) + g[1]/(2*dx) ) / h[-2]
        A[N-1,N] = -dt*( (h[-2]+h[1])*f[1]/(2*dx*dx) - g[1]/(2*dx) ) / h[1] 
        A[0,-1] = -dt*( (h[-2]+h[1])*f[-2]/(2*dx*dx) + g[-2]/(2*dx) ) / h[-2]
        A = A.tocsr()
        Y = np.concatenate((W[1,:], W[2,:]))
        tmp = spsolve(A,Y)
        W[1,:] = tmp[:N]
        W[2,:] = tmp[N:]

    ax4.plot(1000*X,1000*W[0,:])
    ax4.fill_between(1000*X, 1000*W[0,:], color='#539ecd')
    ax4.set_xlim([1000*x_debut, 1000*x_fin])
    ax4.set_ylim([0, 5.5])
    ax4.set_xlabel("x (mm)")
    ax4.set_ylabel("h (mm)")
    
    plt.show(block=True)
    
