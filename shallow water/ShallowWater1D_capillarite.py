import numpy as np
from scipy.sparse import diags, lil_matrix
from scipy.sparse.linalg import spsolve
from matplotlib import pyplot as plt
import argparse

"""
Simulation de shallow water avec capillarité linéarisée ou non linéarisée et des conditions limites périodiques
"""

# Constantes du système
sigma = 0.0728 # N/m
rho = 1000 # kg/m^3
nu = 1e-6 # m^2/s
gr = 9.81 # m/s^2
h1 = 2.725e-3 # m
h0 = h1
b0 = 4.29193
b = 1.5 * h1

# Domaine de calcul
x_debut = -50e-3; x_fin = 50e-3 # m
T = 5e-3 # s
    
# Paramètres du schéma
N = 1000
CFL = 0.01
implicite = True # Type de schéma pour la phase de diffusion : True = implicite / False = explicite
quadratic = False # Type de capillarite : True = quadratic / False = non linear

# Paramètres d'affichage
affichage = False # True / False
anim = False # True / False
anim_nb_img = 100 # Nombre d'images à afficher durant toute la simulation
tps_pause = 0.01 # Temps d'attente après l'affichage de la solution en seconde

def flux_rusanov(W):
    """
    Calcule le flux de Rusanov associé a W
    """
    W_etendu = np.column_stack((W[:,-1],W,W[:,0]))
    u = W_etendu[1,:] / W_etendu[0,:]
    FW = np.zeros(np.shape(W_etendu)) # FW = F(W)
    FW[0,:] = W_etendu[1,:]
    FW[1,:] = u * W_etendu[1,:] + 0.5*gr*(W_etendu[0,:] * W_etendu[0,:])
    FW[2,:] = u * W_etendu[2,:]

    max_abs_lambda = np.abs(u) + np.sqrt( gr * W_etendu[0,:])
    lambda_rusanov = np.amax([max_abs_lambda[:-1], max_abs_lambda[1:]], axis=0)

    F = 0.5*(FW[:,1:] + FW[:,:-1]) - lambda_rusanov * (0.5 * (W_etendu[:,1:] - W_etendu[:,:-1]))
    return F


if (__name__ == "__main__"):
    # Choix du nombre de mailles et du type de capillarité en arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--N', type=int, help='Nombre de mailles')
    parser.add_argument('--capillarity', type=str, help='Choix du type de capillarite')
    args = parser.parse_args()
    if args.N :
        N = args.N
    if args.capillarity :
        if args.capillarity == "nonlinear":
            quadratic = False
        elif args.capillarity == "quadratic":
            quadratic = True

    # Initialisation
    dx = (x_fin-x_debut)/N
    X = np.linspace(x_debut+dx/2,x_fin-dx/2,N)
    h = h0 + h1 * np.exp(- (X*X)/(2*((b/b0)**2)))
    u = np.zeros(N)
    dx_h = h1 * (-X/((b/b0)**2)) * np.exp(- (X*X)/(2*((b/b0)**2)))
    W = np.zeros([3,N])
    W[0,:] = h
    W[1,:] = h * u
    if quadratic :
        W[2,:] = np.sqrt(sigma/rho) * np.sqrt(h) * dx_h
    else :
        W[2,:] = (np.sqrt(2) / np.sqrt(1+np.sqrt(1+dx_h*dx_h))) * np.sqrt(sigma/rho) * np.sqrt(h) * dx_h

    # Energie initiale
    v = W[2,:] / W[0,:]
    energie = [dx * np.sum( 0.5*W[0,:]*(u*u+v*v) + 0.5 * gr * W[0,:]*W[0,:] )]
    
    if affichage and anim :
        # Affichage initiale
        plt.ion()
        fig, [ax1,ax2,ax3,ax4] = plt.subplots(1,4)
        ax1.set_title("Densite : h")
        ax2.set_title("Vitesse : u")
        ax3.set_title("Vitesse auxiliaire : v")
        ax4.set_title("Energie")
        plot1, = ax1.plot(X,W[0,:],"-b")
        plot2, = ax2.plot(X,u,"-b")
        plot3, = ax3.plot(X,v,"-b")
        plt.pause(tps_pause)
        nb_img = 1
    
    t = 0
    temps = [t]
    while(t<T) :
        dt=CFL*dx/(2* np.max(np.abs(W[1,:]/W[0,:]) + np.sqrt( gr * W[0,:])) )
        # Permet à la simulation de se terminer au temps T exact
        if t+dt < T :
            t += dt
        else :
            dt = T-t
            t = T
        temps.append(t)
        
        # Phase hyperbolique
        F = flux_rusanov(W)
        W = W - (dt/dx)*(F[:, 1:] - F[:, :-1])
        
        # Phase de diffusion
        W_etendu = np.column_stack((W[:,-1],W,W[:,0]))
        h = W_etendu[0,:]
        u = W_etendu[1,:] / W_etendu[0,:]
        v = W_etendu[2,:] / W_etendu[0,:]
        if quadratic :
            f = np.sqrt(sigma/rho) * np.sqrt(h)
            g = 0.5 * h * v
        else :
            f = np.sqrt(sigma/rho) * np.sqrt(h) / np.sqrt(1+(rho*h)/(4*sigma)*(v*v)) * (1 - (rho*h) / (4*sigma) * (v*v) / (1 + (rho*h)/(2*sigma)*(v*v)))
            g = 0.5 * h * v / (1+(rho*h)/(2*sigma)*(v*v))
        
        if implicite : #implicite
            diag1 = -dt*( -(h[1:-2]+h[2:-1])*f[2:-1]/(2*dx*dx) + g[2:-1]/(2*dx) ) / h[1:-2]
            diag2 = -dt*( (h[:-2] + 2*h[1:-1] + h[2:])*f[1:-1]/(2*dx*dx) ) / h[1:-1] 
            diag3 = np.concatenate(([0], -dt*( -(h[1:-2]+h[2:-1])*f[1:-2]/(2*dx*dx) - g[1:-2]/(2*dx) ) / h[2:-1]  ,  [0]))
            diag4 = np.ones(2*(N))
            diag5 = np.concatenate(([0],  -dt*( (h[1:-2]+h[2:-1])*f[1:-2]/(2*dx*dx) + g[1:-2]/(2*dx) ) / h[1:-2],  [0]))
            diag6 = -dt*( -(h[:-2] + 2*h[1:-1] + h[2:])*f[1:-1]/(2*dx*dx) ) / h[1:-1]           
            diag7 = -dt*( (h[1:-2]+h[2:-1])*f[2:-1]/(2*dx*dx) - g[2:-1]/(2*dx) ) / h[2:-1]
            A = diags([diag1, diag2, diag3, diag4, diag5, diag6, diag7],
                      [-(N+1),-N,-(N-1),0,N-1,N,N+1])
            A = lil_matrix(A)
            A[-1,0] =  -dt*(-(h[-2]+h[1])*f[-2]/(2*dx*dx) - g[-2]/(2*dx) ) / h[1]
            A[N,N-1] = -dt*(-(h[-2]+h[1])*f[1]/(2*dx*dx) + g[1]/(2*dx) ) / h[-2]
            A[N-1,N] = -dt*( (h[-2]+h[1])*f[1]/(2*dx*dx) - g[1]/(2*dx) ) / h[1] 
            A[0,-1] = -dt*( (h[-2]+h[1])*f[-2]/(2*dx*dx) + g[-2]/(2*dx) ) / h[-2]
            A = A.tocsr()
            Y = np.concatenate((W[1,:], W[2,:]))
            tmp = spsolve(A,Y)
            W[1,:] = tmp[:N]
            W[2,:] = tmp[N:]
        
        else : # explicite
            tmp_var2_1 = (0.5/(dx*dx)) * (h[:-1] + h[1:]) * (f[1:]*v[1:] - f[0:-1]*v[:-1])
            tmp_var2_2 = (0.5/dx) * g * v
            tmp_var3_1 = (0.5/(dx*dx)) * (h[:-1]+h[1:]) * (u[1:]-u[:-1]) 
            W[1,:] = W[1,:] + dt*(tmp_var2_1[1:] - tmp_var2_1[:-1] - tmp_var2_2[2:] + tmp_var2_2[:-2] )
            W[2,:] = W[2,:] + dt*( -f[1:-1]*(tmp_var3_1[1:]-tmp_var3_1[:-1]) - (0.5/dx)*g[1:-1]*(u[2:]-u[:-2]))
        
        # Calcul d'energie
        u = W[1,:] / W[0,:]
        v = W[2,:] / W[0,:]
        energie.append( dx * np.sum( 0.5*W[0,:]*(u*u+v*v) + 0.5 * gr * W[0,:]*W[0,:] ))

        # Affichage du résultat
        if (affichage and anim and t > (nb_img*T/anim_nb_img)):
            u = W[1,:] / W[0,:]
            v = W[2,:] / W[0,:]
            plot1.set_ydata(W[0,:])
            plot2.set_ydata(u)
            plot3.set_ydata(v)
            min_h = min(W[0,:]); max_h = max(W[0,:])
            ax1.set_ylim([min_h-0.05*(max_h-min_h), max_h+0.05*(max_h-min_h)])
            min_u = min(u); max_u = max(u)
            ax2.set_ylim([min_u-0.05*(max_u-min_u), max_u+0.05*(max_u-min_u)])
            min_v = min(v); max_v = max(v)
            ax3.set_ylim([min_v-0.05*(max_v-min_v), max_v+0.05*(max_v-min_v)])
            nb_img += 1
            plt.pause(tps_pause)

    # Affichage du résultat final
    if affichage :
        u = W[1,:] / W[0,:]
        v = W[2,:] / W[0,:]
        if anim : 
            plot1.set_ydata(W[0,:])
            plot2.set_ydata(u)
            plot3.set_ydata(v)
            ax4.plot(temps,energie,"-b")
            min_h = min(W[0,:]); max_h = max(W[0,:])
            ax1.set_ylim([min_h-0.05*(max_h-min_h), max_h+0.05*(max_h-min_h)])
            min_u = min(u); max_u = max(u)
            ax2.set_ylim([min_u-0.05*(max_u-min_u), max_u+0.05*(max_u-min_u)])
            min_v = min(v); max_v = max(v)
            ax3.set_ylim([min_v-0.05*(max_v-min_v), max_v+0.05*(max_v-min_v)])
            ax4.set_ylim([min(energie), max(energie)])
            plt.pause(0.1)
            zoom = input("zoom : ")
            ax1.set_xlim([0, 20e-3])
            ax2.set_xlim([0, 20e-3])
            ax3.set_xlim([0, 20e-3])
            ax1.set_ylim([2e-3, 4e-3])
            ax2.set_ylim([-0.2, 0.2])
            ax3.set_ylim([-0.2, 0.2])
            plt.show(block=True)
        else : 
            plt.ioff()
            plt.figure()
            plt.subplot(1,4,1)
            plt.plot(X, W[0,:], "b")
            plt.title("Densite : h")
            plt.subplot(1,4,2)
            plt.plot(X, u, "b")
            plt.title("Vitesse : u")
            plt.subplot(1,4,3)
            plt.plot(X, v, "b")
            plt.title("Vitesse auxiliaire : v")
            plt.subplot(1,4,4)
            plt.plot(temps, energie, "b" )
            plt.title("Energie")
            plt.show()

    # Enregistrement des résultats
    if quadratic :
        nom_fichier = "SW_quadratic_" + str(N)
    else :
        nom_fichier = "SW_nonlinear_" + str(N)
    np.save("resultats_SW/" + nom_fichier,
               np.row_stack([X,W]))