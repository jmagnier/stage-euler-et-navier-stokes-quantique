import numpy as np
from matplotlib import pyplot as plt

"""
Simulation de shallow water sans capillarité avec conditions limites périodiques
"""

# Constantes du système
gr = 9.81 # m/s^2

# Domaine de calcul
x_debut = -100; x_fin = 100 # m
T = 15 # s

# Paramètres du schéma
N = 1000
CFL = 0.5

# Paramètres d'affichage
anim = False # True / False
anim_nb_img = 100 # Nombre d'images à afficher durant toute la simulation
tps_pause = 0.01 # Temps d'attente après l'affichage de la solution en seconde

def flux_rusanov(W):
    """
    Calcule le flux de Rusanov associe a W
    """
    W_etendu = np.column_stack((W[:,-1],W,W[:,0]))
    u = W_etendu[1,:] / W_etendu[0,:]
    FW = np.zeros(np.shape(W_etendu)) # FW = F(W)
    FW[0,:] = W_etendu[1,:]
    FW[1,:] = u * W_etendu[1,:] + 0.5*gr*(W_etendu[0,:] * W_etendu[0,:])

    max_abs_lambda = np.abs(u) + np.sqrt( gr * W_etendu[0,:])
    lambda_rusanov = np.amax([max_abs_lambda[:-1], max_abs_lambda[1:]], axis=0)

    F = 0.5*(FW[:,1:] + FW[:,:-1]) - lambda_rusanov * (0.5 * (W_etendu[:,1:] - W_etendu[:,:-1]))
    return F


if (__name__ == "__main__"):
    # Initialisation
    dx = (x_fin-x_debut)/N
    X = np.linspace(x_debut+dx/2,x_fin-dx/2,N)
    h = 1 + 0.03 * np.exp(- (X*X)/(310))
    u = np.zeros(N)
    W = np.zeros([2,N])
    W[0,:] = h
    W[1,:] = h * u
    
    if anim :
        # Affichage initiale
        plt.ion()
        fig, [ax1,ax2] = plt.subplots(1,2)
        ax1.set_title("Densite : h")
        ax2.set_title("Vitesse : u")
        plot1, = ax1.plot(X,W[0,:],"-b")
        plot2, = ax2.plot(X,u,"-b")
        plt.pause(tps_pause)
        nb_img = 1
    
    t = 0
    temps = [t]
    while(t<T) :
        dt=CFL*dx/(2* np.max(np.abs(W[1,:]/W[0,:]) + np.sqrt( gr * W[0,:])) )
        t += dt
        temps.append(t)
        
        # Phase hyperbolique
        F = flux_rusanov(W)
        W = W - (dt/dx)*(F[:, 1:] - F[:, :-1])
        
        # Affichage du résultat
        if (anim and t > (nb_img*T/anim_nb_img)):
            u = W[1,:] / W[0,:]
            plot1.set_ydata(W[0,:])
            plot2.set_ydata(u)
            #min_h = min(W[0,:]); max_h = max(W[0,:])
            #ax1.set_ylim([min_h-0.05*(max_h-min_h), max_h+0.05*(max_h-min_h)])
            #min_u = min(u); max_u = max(u)
            #ax2.set_ylim([min_u-0.05*(max_u-min_u), max_u+0.05*(max_u-min_u)])
            nb_img += 1
            plt.pause(tps_pause)
      
    #Affichage du résultat final
    u = W[1,:] / W[0,:]
    if anim : 
        plot1.set_ydata(W[0,:])
        plot2.set_ydata(u)
        #min_h = min(W[0,:]); max_h = max(W[0,:])
        #ax1.set_ylim([min_h-0.05*(max_h-min_h), max_h+0.05*(max_h-min_h)])
        #min_u = min(u); max_u = max(u)
        #ax2.set_ylim([min_u-0.05*(max_u-min_u), max_u+0.05*(max_u-min_u)])
        plt.show(block=True)
    else : 
        plt.ioff()
        plt.figure()
        plt.subplot(1,2,1)
        plt.plot(X, W[0,:], "b")
        plt.title("Densite : h")
        plt.subplot(1,2,2)
        plt.plot(X, u, "b")
        plt.title("Vitesse : u")
        plt.show()
 
    plt.ioff()
    plt.subplot(1,2,1)
    plt.plot(X,h)
    plt.fill_between(X, h, color='#539ecd')
    plt.xlim([-100,100])
    plt.ylim([0.99, 1.04])
    plt.xlabel("x (m)")
    plt.ylabel("h (m)")
    plt.subplot(1,2,2)
    plt.plot(X,W[0,:])
    plt.fill_between(X, W[0,:], color='#539ecd')
    plt.xlim([-100,100])
    plt.ylim([0.99, 1.04])
    plt.xlabel("x (m)")
    plt.ylabel("h (m)")
    plt.show(block=True)