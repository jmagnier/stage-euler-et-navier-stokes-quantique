import numpy as np
from matplotlib import pyplot as plt
import argparse

"""
Comparaison des simulations Shallow Water avec une capillarité linearisée et capillarité non linearisée réalisées à partir ShallowWater1D_capillarite.py 
"""

# Nombre de mailles des simulations à comparer
N = 1000 

if (__name__ == "__main__"):
    # Choix du nombre de mailles en paramètre de l'exécutable
    parser = argparse.ArgumentParser()
    parser.add_argument('--N', type=int, help='Nombre de mailles')
    args = parser.parse_args()
    if args.N :
        N = args.N
    
    # Lecture des données
    donnees_quadratic = np.load("resultats_SW/SW_quadratic_" + str(N) + ".npy")
    donnees_nonlinear = np.load("resultats_SW/SW_nonlinear_" + str(N) + ".npy")

    X_quadratic = donnees_quadratic[0,:]
    X_nonlinear = donnees_nonlinear[0,:]
    if not np.allclose(X_quadratic, X_nonlinear) :
        raise("Les mailles des fichiers 'quadratic_" + str(N) + "_X.txt' et 'nonlinear_" + str(N) + "_X.txt' ne correspondent pas")
    else : 
        X=X_quadratic * 1000 # milimètres en abscisse

    # Calculs de h,u et v à partir des variables conservatives
    h_quadratic = donnees_quadratic[1,:]
    u_quadratic = donnees_quadratic[2,:] / donnees_quadratic[1,:]
    v_quadratic = donnees_quadratic[3,:] / donnees_quadratic[1,:]
    h_nonlinear = donnees_nonlinear[1,:]
    u_nonlinear = donnees_nonlinear[2,:] / donnees_nonlinear[1,:]
    v_nonlinear = donnees_nonlinear[3,:] / donnees_nonlinear[1,:]

    # Configuration de pyplot pour l'affichage du texte mathématique
    params = {'mathtext.default': 'regular' }
    plt.rcParams.update(params)

    # Affichage des densités h
    plt.figure()
    plt.subplot(2,2,1)
    plt.plot(X,h_quadratic *1000, "-b", X,h_nonlinear *1000, "-r")
    plt.xlabel("x (mm)")
    plt.ylabel("h (mm)")
    plt.xlim([0,20])
    plt.ylim([2, 4])
    plt.legend(["Capillarité linéarisée", "Capillarité non linéarisée"])

    # Affichage de l'erreur relative entre les deux modèles sur h
    plt.subplot(2,2,2)
    plt.plot(X,abs(h_quadratic-h_nonlinear)/abs(h_nonlinear),"-k")
    plt.xlabel("x (mm)")
    plt.ylabel("$|h_{lin} - h_{nonlin}|$ / $|h_{nonlin}|$")
    plt.xlim([0,20])
    plt.ylim([0, 0.15])

    # Affichage des vitesses u
    plt.subplot(2,2,3)
    plt.plot(X,u_quadratic, "-b", X,u_nonlinear, "-r")
    plt.xlabel("x (mm)")
    plt.ylabel("u (m/s)")
    plt.xlim([0,20])
    plt.ylim([-0.2, 0.2])    
    
    # Affichage des vitesses auxiliaires v
    plt.subplot(2,2,4)
    plt.plot(X,v_quadratic, "-b", X,v_nonlinear, "-r")
    plt.xlabel("x (mm)")
    plt.ylabel("v (m/s)")
    plt.xlim([0,20])
    plt.ylim([-0.2, 0.2])
    plt.show()
