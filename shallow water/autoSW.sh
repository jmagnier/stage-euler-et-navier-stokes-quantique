#!bin/sh
python3 ShallowWater1D_capillarite.py --N 8000 --capillarity nonlinear
python3 ShallowWater1D_capillarite.py --N 16000 --capillarity quadratic
python3 ShallowWater1D_capillarite.py --N 16000 --capillarity nonlinear
