import numpy as np
from scipy.sparse import diags, block_diag, bmat, eye
from scipy.sparse.linalg import spsolve
from matplotlib import pyplot as plt

"""
Simulation de Navier Stokes quantique 2D avec des conditions limites périodiques
"""

# Constantes du systeme
gamma = 1.4 
epsilon = 1
nu = 1

# Domaine de calcul
x_debut = 0; x_fin = 30
y_debut = 0; y_fin = 30
T = 1 # s

# Paramètres du schéma
Nx = 100
Ny = 100
CFL = 0.5

# Paramètre de la densite initiale minimale
inf_rho = 1

# Paramètres d'affichage
anim = False # True / False
enregistrer_anim = True # True / False
anim_nb_img = 100 # Nombre d'images à afficher durant toute la simulation 
tps_pause = 0.01 # Temps d'attente après l'affichage de la solution en seconde


def flux_rusanov_x(W):
    """
    Calcule le flux de Rusanov dans l'axe x associé à W
    """
    ux = W[1,:,:] / W[0,:,:]
    FW = np.zeros(np.shape(W)) # FW = F(W)
    FW[0,:,:] = W[1,:,:]
    FW[1,:,:] = ux * W[1,:,:] + np.power(W[0,:,:], gamma)
    FW[2,:,:] = ux * W[2,:,:]
    FW[3,:,:] = ux * W[3,:,:]
    FW[4,:,:] = ux * W[4,:,:]

    max_abs_lambda = np.abs(ux) + np.sqrt(gamma)*np.power(W[0,:,:], 0.5*(gamma-1))
    lambda_rusanov = np.amax([max_abs_lambda[:,:-1], max_abs_lambda[:,1:]], axis=0)
    F = 0.5*(FW[:,:,1:] + FW[:,:,:-1]) - 0.5 * lambda_rusanov * (W[:,:,1:] - W[:,:,:-1])
    return F


def flux_rusanov_y(W):
    """
    Calcule le flux de Rusanov dans l'axe y associé à W
    """
    uy = W[2,:,:] / W[0,:,:]
    FW = np.zeros(np.shape(W)) # FW = F(W)
    FW[0,:,:] = W[2,:,:]
    FW[1,:,:] = uy * W[1,:,:] 
    FW[2,:,:] = uy * W[2,:,:] + np.power(W[0,:,:], gamma)
    FW[3,:,:] = uy * W[3,:,:]
    FW[4,:,:] = uy * W[4,:,:]

    max_abs_lambda = np.abs(uy) + np.sqrt(gamma)*np.power(W[0,:,:], 0.5*(gamma-1))
    lambda_rusanov = np.amax([max_abs_lambda[:-1,:], max_abs_lambda[1:,:]], axis=0)
    F = 0.5*(FW[:,1:,:] + FW[:,:-1,:]) - 0.5 * lambda_rusanov * (W[:,1:,:] - W[:,:-1,:])
    return F


if (__name__ == "__main__"):
    # Initialisation
    dx = (x_fin-x_debut)/Nx
    dy = (y_fin-y_debut)/Ny
    x = np.linspace(x_debut+dx/2,x_fin-dx/2,Nx)
    y = np.linspace(y_debut+dy/2,y_fin-dy/2,Ny)
    X,Y = np.meshgrid(x,y)
    x_milieu = 0.5*(x_debut+x_fin); y_milieu = 0.5*(y_debut+y_fin)
    N_mailles = Nx*Ny
    demi_Nx = int(Nx / 2)


    rho = inf_rho + np.exp(-((X-x_milieu)**2+(Y-y_milieu)**2))
    dx_rho = -2*(X-x_milieu)*np.exp(-((X-x_milieu)**2+(Y-y_milieu)**2))
    dy_rho = -2*(Y-y_milieu)*np.exp(-((X-x_milieu)**2+(Y-y_milieu)**2))
    ux = np.zeros([Ny,Nx])
    uy = np.zeros([Ny,Nx])
    W = np.zeros([5,Ny,Nx])
    W[0,:] = rho
    W[1,:] = rho * ux
    W[2,:] = rho * uy
    W[3,:] = epsilon * dx_rho
    W[4,:] = epsilon * dy_rho

    if anim :
        # Affichage initiale
        plt.ion()
        fig, ax = plt.subplots(1,1)
        im = ax.imshow(rho, interpolation='nearest',  
                            vmin=max(0, inf_rho*0.8),
                            vmax=np.max(rho),
                            extent=[x_debut, x_fin, y_debut, y_fin],
                            cmap='jet')
        cb = plt.colorbar(im)
        ax.set_title("Shallow Water 2D sans capillarité")
        plt.pause(tps_pause)
    if enregistrer_anim :
        data_anim = [X,Y,W[0]]
    nb_img = 1

    t = 0
    temps = [t]
    min_rho0 = min(W[0])
    min_rho = min_rho0
    while(t<T) :
        drho_pi = np.sqrt(gamma)*np.power(W[0,:,:], 0.5*(gamma-1))
        dt=CFL*dx/( np.max([np.abs(W[1,:,:]/W[0,:,:]) + drho_pi,
                            np.abs(W[2,:,:]/W[0,:,:]) + drho_pi] ))
        t += dt
        temps.append(t)
        
        # Phase hyperbolique
        F = flux_rusanov_x(np.concatenate( (W[:,:,[-1]],W,W[:,:,[0]]), axis=2 ))
        G = flux_rusanov_y(np.concatenate( (W[:,[-1],:],W,W[:,[0],:]), axis=1 ))
        W = W - (dt/dx)*(F[:,:,1:] - F[:,:,:-1]) - (dt/dy)*(G[:,1:,:] - G[:,:-1,:])

        # Phase de dffusion
        rho = W[0]
        # Ajout des coefficents adjacents par périodicité
        rho = np.concatenate((rho[[-1],:],rho,rho[[0],:]),axis=0)
        rho = np.concatenate((rho[:,[-1]],rho,rho[:,[0]]),axis=1)

        # Construction des blocs
        blocs_A13 = []
        for ind_y in range(1,Ny+1):
            diag_inf = ( rho[ind_y, 1:-2] + rho[ind_y, 2:-1] ) / rho[ind_y, 1:-2]
            diag_mid = -( rho[ind_y, 0:-2] + 2*rho[ind_y, 1:-1] + rho[ind_y, 2:]) / rho[ind_y, 1:-1]
            diag_sup = ( rho[ind_y, 1:-2] + rho[ind_y, 2:-1] ) / rho[ind_y, 2:-1 ]
            bloc = diags([diag_inf,diag_mid,diag_sup], [-1,0,1], format='lil')
            bloc[0,-1] = ( rho[ind_y, 0] + rho[ind_y, 1] ) / rho[ind_y,0]
            bloc[-1,0] = ( rho[ind_y, -2] + rho[ind_y, -1] ) / rho[ind_y,-1]
            blocs_A13.append(bloc)
        A13 = block_diag(blocs_A13, format='lil')



        A14 = diags(np.zeros(N_mailles), format='lil') 
        # Remplissage des blocs inférieurs de A14
        for ind_y in range(2,Ny+1) :
            diag_inf = rho[ind_y-1, 2:-1] / rho[ind_y-1, 1:-2]
            diag_sup = -rho[ind_y-1, 1:-2] / rho[ind_y-1, 2:-1]
            bloc = diags([diag_inf, diag_sup],[-1, 1],format='lil')
            bloc[0,-1] = rho[ind_y-1, 1] / rho[ind_y-1, 0]
            bloc[-1,0] = -rho[ind_y-1, -2] / rho[ind_y-1, -1]
            A14[(ind_y-1)*Nx : ind_y*Nx, (ind_y-2)*Nx : (ind_y-1)*Nx ] = bloc
        # Bloc de la condition périodique
        diag_inf = rho[0, 2:-1] / rho[0, 1:-2]
        diag_sup = -rho[0, 1:-2] / rho[0, 2:-1]
        bloc = diags([diag_inf, diag_sup],[-1, 1],format='lil')
        bloc[0,-1] = rho[0, 1] /  rho[0, 0]
        bloc[-1,0] = -rho[0, -2] / rho[0, -1]
        A14[:Nx,-Nx:] = bloc
        # Remplissage des blocs supérieurs de A14
        for ind_y in range(1,Ny) :
            diag_inf = -rho[ind_y+1, 2:-1] / rho[ind_y+1, 1:-2]
            diag_sup = rho[ind_y+1, 1:-2] / rho[ind_y+1, 2:-1]
            bloc = diags([diag_inf, diag_sup],[-1, 1],format='lil')
            bloc[0,-1] = -rho[ind_y+1, 1] / rho[ind_y+1, 0]
            bloc[-1,0] = rho[ind_y+1, -2] / rho[ind_y+1, -1]
            A14[(ind_y-1)*Nx : ind_y*Nx, ind_y*Nx : (ind_y+1)*Nx ] = bloc
        # Bloc de la condition périodique
        diag_inf = -rho[Ny+1, 2:-1] / rho[Ny+1, 1:-2]
        diag_sup = rho[Ny+1, 1:-2] / rho[Ny+1, 2:-1]
        bloc = diags([diag_inf, diag_sup],[-1, 1],format='lil')
        bloc[0,-1] = -rho[Ny+1, 1] / rho[Ny+1, 0]
        bloc[-1,0] = rho[Ny+1, -2] / rho[Ny+1, -1]
        A14[-Nx:,:Nx ] = bloc



        A23 = diags(np.zeros(N_mailles), format='lil') 
        # Remplissage des blocs inférieurs de A23
        for ind_y in range(2,Ny+1) :
            diag_inf = rho[ind_y, 1:-2] / rho[ind_y-1, 1:-2]
            diag_sup = -rho[ind_y, 2:-1] / rho[ind_y-1, 2:-1]
            bloc = diags([diag_inf, diag_sup],[-1, 1],format='lil')
            bloc[0,-1] = rho[ind_y, 0] / rho[ind_y-1, 0]
            bloc[-1,0] = -rho[ind_y, -1] / rho[ind_y-1, -1]
            A23[(ind_y-1)*Nx : ind_y*Nx, (ind_y-2)*Nx : (ind_y-1)*Nx ] = bloc
        # Bloc de la condition périodique
        diag_inf = rho[1, 1:-2] / rho[0, 1:-2]
        diag_sup = -rho[1, 2:-1] / rho[0, 2:-1]
        bloc = diags([diag_inf, diag_sup],[-1, 1],format='lil')
        bloc[0,-1] = rho[1, 0] / rho[0, 0]
        bloc[-1,0] = -rho[1, -1] / rho[0, -1]
        A23[:Nx,-Nx:] = bloc
        # Remplissage des blocs supérieurs de A23
        for ind_y in range(1,Ny) :
            diag_inf = -rho[ind_y, 1:-2] / rho[ind_y+1, 1:-2]
            diag_sup = rho[ind_y, 2:-1] / rho[ind_y+1, 2:-1]
            bloc = diags([diag_inf, diag_sup],[-1, 1],format='lil')
            bloc[0,-1] = -rho[ind_y, 0] / rho[ind_y+1, 0]
            bloc[-1,0] = rho[ind_y, -1] / rho[ind_y+1, -1]
            A23[(ind_y-1)*Nx : ind_y*Nx, ind_y*Nx : (ind_y+1)*Nx ] = bloc
        # Bloc de la condition périodique
        diag_inf = -rho[Ny, 1:-2] / rho[Ny+1, 1:-2]
        diag_sup = rho[Ny, 2:-1] / rho[Ny+1, 2:-1]
        bloc = diags([diag_inf, diag_sup],[-1, 1],format='lil')
        bloc[0,-1] = - rho[Ny, 0] / rho[Ny+1, 0]
        bloc[-1,0] = rho[Ny, -1] / rho[Ny+1, -1]
        A23[-Nx:,:Nx ] = bloc



        rho_etendu_y = np.concatenate([W[0,[-1],:],W[0],W[0,[1],:]],axis=0).ravel()
        diag_inf = (rho_etendu_y[:-2*Nx] + rho_etendu_y[Nx:-Nx]) / rho_etendu_y[:-2*Nx]
        diag_mid = -(rho_etendu_y[:-2*Nx] + 2*rho_etendu_y[Nx:-Nx] + rho_etendu_y[2*Nx:]) / rho_etendu_y[Nx:-Nx]
        diag_sup = (rho_etendu_y[Nx:-Nx] + rho_etendu_y[2*Nx:]) / rho_etendu_y[2*Nx:]
        A24 = diags([diag_sup[-Nx:], diag_inf[Nx:], diag_mid, diag_sup[:-Nx], diag_inf[:Nx]],
                    [-(N_mailles-Nx), -Nx, 0, Nx, N_mailles-Nx], format='lil')



        # Assemblage des blocs
        cste_dtdxeps = epsilon*dt/(2*dx*dx)
        cste_dtdxdyeps = epsilon*dt/(4*dx*dy)
        cste_dtdyeps = epsilon*dt/(2*dy*dy)
        cste_dtdxnu = nu*dt/(2*dx*dx)
        cste_dtdxdynu = nu*dt/(4*dx*dy)
        cste_dtdynu = nu*dt/(2*dy*dy)
        M = bmat([[eye(N_mailles) - 2*cste_dtdxnu*A13 - cste_dtdynu*A24, -cste_dtdxdynu*A23, -cste_dtdxeps * A13, -cste_dtdxdyeps * A14],
                  [-cste_dtdxdynu*A14, eye(N_mailles) - cste_dtdxnu*A13 - 2*cste_dtdynu*A24, -cste_dtdxdyeps * A23, -cste_dtdyeps * A24],
                  [cste_dtdxeps * A13, cste_dtdxdyeps * A14, eye(N_mailles), None],
                  [cste_dtdxdyeps * A23, cste_dtdyeps * A24, None, eye(N_mailles)]],
                 format="csr")


        W_systeme = W[1:,:,:].ravel() 
        tmp = spsolve(M,W_systeme)
        W[1] = tmp[:N_mailles].reshape(Ny,Nx)
        W[2] = tmp[N_mailles:2*N_mailles].reshape(Ny,Nx)
        W[3] = tmp[2*N_mailles:3*N_mailles].reshape(Ny,Nx)
        W[4] = tmp[3*N_mailles:].reshape(Ny,Nx)

        
        # Calcul du minimum de la densité durant toute la simulation
        min_rho = min(min(W[0]), min_rho)

        if t >= (nb_img*T/anim_nb_img) : 
            # Affichage du résultat
            if anim :
                im.set_data(W[0,:,:]) 
                plt.pause(tps_pause)        
            # Stockage du résultat
            if enregistrer_anim :
                data_anim.append(W[0])
            nb_img += 1

    #Affichage du resultat final
    if anim : 
        im.set_data(W[0,:,:]) 
        plt.show(block=True)
    else : 
        plt.ioff()
        fig = plt.figure(figsize = plt.figaspect(1))
        ax = fig.add_subplot(1,1,1,projection='3d')
        surf = ax.plot_surface(X,Y,W[0],cmap='magma')
        plt.show(block=True)

    fig = plt.figure(figsize = plt.figaspect(1))
    ax = fig.add_subplot(1,1,1,projection='3d')
    surf = ax.plot_surface(X[:,:demi_Nx],Y[:,:demi_Nx],W[0,:,:demi_Nx],cmap='magma')
    ax.set_title("Coupe de la solution, x < {:.2f}".format(x[demi_Nx]))
    plt.show(block=True)

    if enregistrer_anim :
        data_anim.append(W[0])
        np.save("data/NSQ2D_Nx{:d}_Ny{:d}_eps{:.0f}_nu{:.0f}".format(Nx,Ny,epsilon,nu), data_anim)

    print("Minimum de la densité à l'instant initial : {:e}".format(min_rho0))
    print("Minimum de la densité atteinte durant la simulation : {:e}".format(min_rho))