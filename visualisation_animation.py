import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import tkinter, tkinter.ttk, tkinter.filedialog # librairie pour la création de la fenêtre
from os.path import basename

class Menu():
    def __init__(self):
        # Initialisation de la fenêtre tkinter
        self.root = tkinter.Tk()
        self.root.title("Paramètres d'animation")

        # Création des variables
        self.nomFichier = tkinter.StringVar()
        self.duree = tkinter.DoubleVar(); self.duree.set(5) 
        self.enregistrerMP4 = tkinter.IntVar()
        self.enregistrerGIF = tkinter.IntVar()

        # Création des widget
        self.label_nomFichier = tkinter.Label(self.root, textvariable=self.nomFichier)
        self.button_nomFichier = tkinter.Button(self.root,
                                                text="Choisir un Fichier",
                                                command=self.set_nomFichier)
        self.checkbox_enregistrerMP4 = tkinter.Checkbutton(self.root,
                                                           text="Enregistrer l'animation au format .mp4 (nécessite ffmpeg)",
                                                           variable=self.enregistrerMP4)
        self.checkbox_enregistrerGIF = tkinter.Checkbutton(self.root,
                                                           text="Enregistrer l'animation au format .gif (nécessite imagemagick)",
                                                           variable=self.enregistrerGIF)
        self.grille_duree = tkinter.ttk.Frame(self.root, padding=5) # Création d'une frame permettant d'aligner la description et le champs
        self.entry_duree = tkinter.Entry(self.grille_duree,
                                       width=3,
                                       textvariable=self.duree)
        self.entry_duree.grid(row=0, column=0)
        self.titre_duree = tkinter.Label(self.grille_duree,
                                       text="Durée de l'animation en secondes")
        self.titre_duree.grid(row=0, column=1)
        self.button_valider = tkinter.Button(self.root, 
                                        text="Valider", 
                                        command=self.root.destroy)

        # Placement des widget
        self.button_nomFichier.pack()
        self.label_nomFichier.pack()
        self.checkbox_enregistrerMP4.pack()
        self.checkbox_enregistrerGIF.pack()
        self.grille_duree.pack()
        self.button_valider.pack()
        self.root.mainloop()

    def set_nomFichier(self):
        self.nomFichier.set(tkinter.filedialog.askopenfilename(initialdir="data",
                                                       filetypes=(("Fichiers Numpy","*.npy"),)))

    def get_nomFichier(self) : 
        return self.nomFichier.get()
    
    def get_enregistrerMP4(self) : 
        return self.enregistrerMP4.get()
    
    def get_enregistrerGIF(self) : 
        return self.enregistrerGIF.get()
    
    def get_duree(self) : 
        return self.duree.get()


def surf_maj(i, W_anim, surf):
    surf[0].remove()
    surf[0] = ax.plot_surface(X, Y, W_anim[i], cmap="magma")

def plot_maj(i, W_anim, plot):
    plot.set_ydata(W_anim[i])


if __name__ == "__main__" :
    param=Menu()
    nomFichier = param.get_nomFichier()
    enregistrerMP4 = param.get_enregistrerMP4()
    enregistrerGIF = param.get_enregistrerGIF()
    duree_anim = param.get_duree()

    data = np.load(nomFichier)
    if data.ndim == 2 :
        X = data[0]
        W_anim = data[1:]
        nb_anim = W_anim.shape[0]
        fig = plt.figure()
        ax = fig.subplots(1,1) 
        ax.set_ylim([W_anim.min(), W_anim.max()])
        plot = plt.plot(X,W_anim[0])[0]
        anim = animation.FuncAnimation(fig, plot_maj, frames=nb_anim, fargs=(W_anim, plot), interval=1000*duree_anim/nb_anim)
    elif data.ndim == 3 :
        X = data[0]; Y = data[1]
        W_anim = data[2:]
        nb_anim = W_anim.shape[0]
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        surf = [ax.plot_surface(X, Y, W_anim[0], cmap="magma", rstride=1, cstride=1)]
        ax.set_zlim(W_anim.min(),W_anim.max())
        anim = animation.FuncAnimation(fig, surf_maj, frames=nb_anim, fargs=(W_anim, surf), interval=1000*duree_anim/nb_anim)
    else :
        raise(ValueError("Mauvaises dimensions de la solution"))

    plt.show()
    
    nomFichierabrg = basename(nomFichier)[:-4]
    if enregistrerMP4 :
        writervideo = animation.FFMpegWriter(fps=nb_anim/duree_anim)
        anim.save('animations/' + nomFichierabrg + ".mp4", writer=writervideo)
    if enregistrerGIF :
        anim.save('animations/' + nomFichierabrg + ".gif", writer="imagemagick")
