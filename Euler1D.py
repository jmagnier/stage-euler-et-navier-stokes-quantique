import numpy as np
from matplotlib import pyplot as plt

"""
Simulation du tube à choc de Sod
"""

# Constantes du système
gamma = 1.4

# Domaine de calcul
x_debut=-5; x_fin=5
T = 1.896

# Etats de la solution initiale
x_discontinu = 0 # x de la discontinuité du problème de riemann
rho_g = float(1); rho_d = float(0.125) # densité à gauche et à droite de la discontinuité
u_g   = float(0); u_d   = float(0) # vitesse à gauche et à droite de la discontinuité
p_g   = float(1); p_d   = float(0.1) # pression à gauche et à droite de la discontinuité

# Parametres du schéma
N = 1000
dx = (x_fin-x_debut)/N
CFL = 0.1

# Parametres d'affichage
anim_nb_img = 20 # > 0
tps_pause = 0.01

def flux_rusanov(W):
    """
    Calcule le flux de Rusanov associé à W
    """
    u = W[1,:] / W[0,:] #vitesse 
    c  = np.sqrt( (gamma*(gamma-1)) * (W[2,:] - 0.5*u*u) ) #vitesse du son

    FW = np.zeros(np.shape(W)) # FW = F(W)
    FW[0,:] = W[1,:]
    FW[1,:] = W[1,:]*u + (gamma-1)*(W[2,:] - 0.5*W[1,:]*u)
    FW[2,:] = u * (gamma*W[2,:] - (0.5*(gamma-1)) * W[1,:]*u)

    max_abs_lambda = np.abs(u) + c
    lambda_rusanov = np.amax([max_abs_lambda[:-1], max_abs_lambda[1:]], axis=0)

    flux = 0.5*(FW[:,1:] + FW[:,:-1]) - lambda_rusanov * (0.5 * (W[:,1:] - W[:,:-1]))
    return flux


if (__name__ == "__main__"):
    # Initialisation
    X = np.linspace(x_debut+dx/2,x_fin-dx/2,N)
    rho = np.where(X<x_discontinu, rho_g, rho_d)
    u = np.where(X<x_discontinu, u_g, u_d)
    p = np.where(X<x_discontinu, p_g, p_d)
    W = np.zeros([3,N])
    W[0,:] = rho
    W[1,:] = rho * u
    W[2,:] = p / (gamma-1) + 0.5*rho*u*u

    # Affichage initiale
    plt.ion()
    fig, [ax1,ax2,ax3] = plt.subplots(1,3)
    ax1.set_title("Densité")
    ax2.set_title("Vitesse")
    ax3.set_title("Pression")
    plot1, = ax1.plot(X,rho,"-b")
    plot2, = ax2.plot(X,u,"-b")
    plot3, = ax3.plot(X,p,"-b")
    plt.pause(tps_pause)

    t = 0
    nb_img = 1
    temps = [t]
    while(t<T) :
        u = W[1,:]/W[0,:]
        c = np.sqrt((gamma*(gamma-1)) * (W[2,:] - 0.5*u*u))
        dt=CFL*dx/(2*np.max(np.abs(u)+c))

        if (t+dt < T):
            t += dt
        else :
            dt = T-t
            t = T
        temps.append(t)

        # Calcul des nouveaux flux
        F = flux_rusanov(W)
        # Calcul de la solution numérique
        W[:, 1:N-1] = W[:, 1:N-1] - (dt/dx)*(F[:, 1:N-1] - F[:, 0:N-2])

        # Affichage du resultat
        if (t > (nb_img*T/anim_nb_img)):
            u = W[1,:] / W[0,:]
            p = (gamma-1) * (W[2,:] - 0.5*W[1,:]*u )
            plot1.set_ydata(W[0,:])
            plot2.set_ydata(u)
            plot3.set_ydata(p)
            ax2.set_ylim([min(u)-0.05,max(u)+0.05])
            nb_img += 1
            plt.pause(tps_pause)

    # Importation de la solution exacte
    data_exact = np.loadtxt("data/solution_euler")

    # Affichage du resultat final
    u = W[1,:] / W[0,:]
    p = (gamma-1) * (W[2,:] - 0.5*W[1,:]*u )
    plot1.set_ydata(W[0,:])
    plot2.set_ydata(u)
    plot3.set_ydata(p)
    ax1.plot(data_exact[:,1],data_exact[:,2], "-r")
    ax2.plot(data_exact[:,1],data_exact[:,4], "-r")
    ax3.plot(data_exact[:,1],data_exact[:,3], "-r")
    plt.pause(0.001)
    ax1.legend(['solution numérique','solution exacte'])
    plt.show(block=True)
