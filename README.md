# Stage Euler et Navier Stokes quantique

Codes Python des simulations effectuées durant mon stage "Méthodes numériques pour la résolution des équations d’Euler et Navier-Stokes quantique".
Les librairies utilisées sont :
- Numpy
- Scipy
- Matplotlib
- Tkinter

Le script **visualation_animation.py** permet de créer et d'enregistrer les animations des solutions générées par les scripts de calculs dans le dossier **data** (si le booléen *enregistrer_anim* dans ces scripts est fixé à *True*).
L'enregistrement de ces animations nécessite le logiciel **ffmpeg** pour le format **.mp4** et **imagemagick** pour le format **.gif**.
